FROM openjdk:8-jre-slim

EXPOSE 9090

RUN mkdir /app

COPY build/libs/VulnerableApp-1.0.0.jar /app/VulnerableApp-1.0.0.jar

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom","-jar","/app/VulnerableApp-1.0.0.jar"]
